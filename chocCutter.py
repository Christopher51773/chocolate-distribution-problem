#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Author: Christopher Ellis
Version: 1.0

There are m chocolate bars of varying (integer) length and n hungry children
who want differing amounts of chocolate (again integer amounts). You can cut
the chocolate bars and the goal is to ensure that every child gets the
amount they want. You can assume that there is always sufficient chocolate
to meet demands.

Write a program to distribute the chocolate so that you make the minimum
number of cuts.

For example, suppose the chocolate bars have lengths {2,5,7} and four
children want {3,2,5,1} then you can solve the problem using 2 cuts.

Example: chocoCutter '{2,5,7}' '{3,2,5,1}'
Result: Hooray all children have been fed! 2 cuts were made.


cellis@christopher-ellis.uk

Usage:
  chocCutter <bars> <children>
  chocCutter -h | --help
  chocCutter --version

Options:
  -h --help                                         Show this screen
  -v --version                                      Show version
"""

# Third Party Imports
from docopt import docopt
import numpy as np
from collections import Counter

# Set usage documentation
if __name__ == "__main__":
    args = docopt(__doc__, version=1.0)

# Constants
CHILDREN = [int(barLength)
            for barLength in args['<children>'][1:-1].split(',')]
BARS = [int(barLength) for barLength in args['<bars>'][1:-1].split(',')]
NO_CHILDREN = len(CHILDREN)
NO_BARS = len(BARS)

# Utility functions


def reduceStatespace(children, bars):
    ''' A utiltiy function to remove exact matches between children and bars'''
    remainingChildren = list((Counter(children) - Counter(bars)).elements())
    remainingBars = list((Counter(bars) - Counter(children)).elements())
    remainingBars.sort(reverse=True)
    remainingChildren.sort(reverse=True)
    return (remainingChildren, remainingBars)


def dynamic01Solver(demand, supply, profit=None):
    '''
    A dynamic programming solution to the subset sum problem
    Note that subset sum is a special case of the 0-1 Knapsack problem
    were the weights equal the profit.
    The function takes a single demand for chocolate and an array of
    bars that can be used to make up the demand.
    The function retruns the first set of bars in the supply array that sum
    to the value of the demand.
    '''
    if profit is None:
        profit = supply
    supply = [0] + supply
    profit = [0] + profit
    rows = len(supply)
    cols = demand + 1
    itemIndexes = []
    solutionMatrix = np.zeros((rows, cols))
    if demand < 0 or len(supply) < 0:
        return itemIndexes
    for i in range(rows):
        for j in range(cols):
            if i == 0 or j == 0:
                continue
            if supply[i] <= j:
                solutionMatrix[i, j] = max(
                    solutionMatrix[i - 1, j],
                    (solutionMatrix[i - 1, j - supply[i]] + profit[i]))
            else:
                solutionMatrix[i, j] = solutionMatrix[i - 1, j]
            if solutionMatrix[i, j] == demand:
                while i > 0 and j > 0:
                    if solutionMatrix[i, j] == solutionMatrix[i - 1, j]:
                        i -= 1
                    else:
                        itemIndexes.append(i - 1)
                        j = j - profit[i]
                        i -= 1
                return itemIndexes
    return itemIndexes


def removePerfectSubsets(needs, supply):
    '''
    A utiltiy function to remove all case where a demand can be met from a
    subset of bars exactly without requiring cut.
    '''
    needs.reverse()
    for i, demand in reversed(list(enumerate(needs))):
        indexes = dynamic01Solver(demand, supply)
        if len(indexes) > 0:
            # Update needs
            del needs[i]
            # Update supply
            for index in sorted(indexes, reverse=True):
                del supply[index]
    needs.reverse()
    return needs, supply


def greedySolver(need, supply, cuts):
    cuts = cuts
    need, supply = reduceStatespace(need, supply)
    need, supply = removePerfectSubsets(need, supply)
    if(len(need) == 0):
        return need, supply, cuts
    score = np.array(supply) - need[0]
    maxScore = score[0]
    if(maxScore > 0):
        need.pop(0)
        supply[0] = score[0]
        cuts += 1
        need, supply = reduceStatespace(need, supply)
        need, supply = removePerfectSubsets(need, supply)
        if(len(need) == 0):
            return need, supply, cuts
    else:
        need[0] -= supply.pop(0)

    return need, supply, cuts


def minCuts(bars, children):
    '''
    The main function to compute the minimum number of cuts required
    to meet the children's demands from the supply of chocolate bars.
    '''
    assert (sum(bars) >= sum(children)
            ), \
        "The amount of chocolate must equal or exceed the childrens demands"
    cuts = 0
    remainingBars = bars
    remainingChildren = children
    hungry = True
    while hungry:
        remainingChildren, remainingBars, cuts = greedySolver(
            remainingChildren, remainingBars, cuts)
        hungry = len(remainingChildren)
    return (cuts, remainingBars, remainingChildren)

# Compute and return result
result = minCuts(BARS, CHILDREN)
print('Hooray all children have been fed! ' +
      str(result[0]) + ' cuts were made.')

