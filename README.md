A greedy dynamic programming approach to the chocolate bar minimisation problem described below.

Note: This repo contains a compiled macosx binary built with PyInstaller so that it can be run without any external dependencies including the python interpreter.


The Problem
==============
There are m chocolate bars of varying (integer) length and n hungry children
who want differing amounts of chocolate (again integer amounts). You can cut
the chocolate bars and the goal is to ensure that every child gets the
amount they want. You can assume that there is always sufficient chocolate to meet demands.

Write a program to distribute the chocolate so that you make the minimum
number of cuts.

For example, suppose the chocolate bars have lengths {2,5,7} and four
children want {3,2,5,1} then you can solve the problem using 2 cuts.

